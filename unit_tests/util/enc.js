const expect = require('chai').expect;
var x = require('../../util/enc.js');
describe('enc.js', function() {
  it('encrypt should be a function', function() {
    expect(typeof x).to.equal('function');
  });
  it('encrypt should encrypt a string', function() {
    expect(x('test123')).to.equal('ef1b4570af8ed0');
  });
});
