var expect = require('chai').expect;
var override = require('../.override.js');
describe('.override.json', function() {
  it('should contain a defined key and a defined pass key', function() {
    expect(override.user).to.not.be.empty;
    expect(override.pass).to.not.be.empty;
    expect(override.env).to.not.be.empty;
  });
});
