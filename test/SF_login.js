var SFlogin = require('../page_objects/SF_login.page.js');
describe('First Attempt', function () {
  this.timeout(300000);
  it('Should navigate to salesforce', function () {
    browser.url('https://test.salesforce.com/');
  });
  it('Should login', function() {
    SFlogin.username.setValue(SFlogin.sfuser);
    SFlogin.password.setValue(SFlogin.sfpass);
    SFlogin.submitbutton.click();
  });
  it('should pause', function () {
    browser.pause(10000);
  });
});
