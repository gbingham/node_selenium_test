var OEtool_login = require('../page_objects/OEtool_login.page.js');
describe('First Attempt', function () {
  this.timeout(300000);
  it('Should navigate to oetools', function () {
    browser.url('https://oetool-' + OEtool_login.env + '.vivint.com');
  });
  it('Should login', function() {
    OEtool_login.username.waitForExist();
    OEtool_login.username.setValue(OEtool_login.oeuser);
    OEtool_login.password.setValue(OEtool_login.oepass);
    OEtool_login.submitbutton.click();
  });
  it('should pause', function () {
    browser.pause(10000);
  });
});
