var Page = require('./page');
var OEstring = {
  devgrnacre: 'dev',
  stg: 'stg',
  prod: '',
};
class LoginPage extends Page {
  constructor() {
    super();
    this.env = OEstring[this.override.env];
    this.oepass = this.dec(this.override.pass);
    this.oeuser = this.override.user;
  }
  get username() {
    return browser.element('#identification');
  }
  get password() {
    return browser.element('#password');
  }
  get submitbutton() {
    return browser.element('#log-in-submit');
  }
  open() {
    super.open('');
  }
}
module.exports = new LoginPage();
