class Page {
	constructor() {
		this.override = require('../.override.js');
		this.dec = require('../lib/dec.js');
	}
	open(path) {
		browser.url('/' + path);
	}
}
module.exports = Page;
