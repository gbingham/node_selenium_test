var Page = require('./page');
class LoginPage extends Page {
  constructor() {
    super();
  }
  get firstName() {
    return browser.element('#ember706');
  }
  get lastName() {
    return browser.element('#ember707');
  }
  get phone() {
    return browser.element('#ember708');
  }
  get address() {
    return browser.element('#ember727');
  }
  get zip() {
    return browser.element('#ember723');
  }
  get submitbutton() {
    return browser.element('#verifyAddressSubmit');
  }
  open() {
    super.open('address-verify');
  }
}
module.exports = new LoginPage();
