var Page = require('./page');
var SFstring = {
  devgrnacre: '.devgrnacre',
  stg: '.stg',
  prod: '',
};
class LoginPage extends Page {
  constructor() {
    super();
    this.sfpass = this.dec(this.override.pass);
    this.env = this.override.env;
    this.sfuser = this.override.user + '@vivint.com' + SFstring[this.env];
  }
  get username() {
    return browser.element('#username');
  }
  get password() {
    return browser.element('#password');
  }
  get submitbutton() {
    return browser.element('#Login');
  }
  open() {
    super.open('login');
  }
}
module.exports = new LoginPage();
