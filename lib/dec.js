var crypto = require('crypto');
var algorithm = 'aes-256-ctr';
var password = 'ip5$VQnvfC0W#6H9$uiS9^yMt';

module.exports = function(text) {
  var decipher = crypto.createDecipher(algorithm, password);
  var dec = decipher.update(text, 'hex', 'utf8');
  dec += decipher.final('utf8');
  return dec;
};
