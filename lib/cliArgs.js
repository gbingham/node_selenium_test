/**
 * cliArgs module used to aquire command line arguments and setup an object based on the arguments for the consumption of the selenium framework
 * @module cliArgs */
var parseArgs = require('minimist');
var argConfig = require('../cliArgs.json');

module.exports = getArgs();
/**
 * args - used to return an object with all defined cli args (cliArgs.json)
 * @requires minimist
 * @return {Object}
 * @see {@link https://github.com/substack/minimist|minimist}
 */
function getArgs() {
  argObj = parseArgs(process.argv);
  var result = {};
  for (var argConfigKey in argConfig) {
    result[argConfigKey] = processArgs(argConfig[argConfigKey]);
  }

  for (var item in result) {
    this[item] = result[item];
  }
  return result;
}
/**
 * processArgs - used as a helper function for the args function, given the flag and expected values, will pass it to the check function for further processing
 */
function processArgs(argConfigObj) {
  for (var flag of argConfigObj.flag) {
    if (argObj[flag]) {
      return check(argObj[flag], argConfigObj.expected);
    }
  }
}
/**
 * check - used as a helper function for the processArgs function, given the arguments and the specified args config (cliArgs.json) will result in assigning the value to the main returned object
 */
function check(item, expect) {
  if (typeof item === 'string') {
    item = item.trim();
  }
  if (typeof item === 'number') {
    item = item.toString();
  }
  var type = typeof expect;
  if (Array.isArray(expect)) {
    for (var expectValue of expect) {
      if (item === expectValue) {
        return item;
      }
    }
  } else if (type === 'string' || type === 'number' || type === 'boolean') {
    return item;
  } else {
    return undefined;
  }
}
